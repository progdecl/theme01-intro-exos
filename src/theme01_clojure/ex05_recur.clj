;; # Exercice 5 : Récursion et collections

(ns theme01-clojure.ex05-recur
  (:use midje.sweet))



;; ## Question 1 : Construction d'un vecteur

(declare inter)


(fact "`inter` construit les bons intervalles."
      (inter 2 5 1) => [2 3 4 5]
      (inter 2 5 2) => [2 4]
      (inter 2 2 10) => [2]
      (inter 3 2 1) => []
      (inter 10 50 10) => [10 20 30 40 50])

;; ## Question 2 : Transformation d'un vecteur

(declare vmap)


(fact "`vmap` applique bien la fonction au vecteur."
      (vmap - [1 2 3 4 5]) => [-1 -2 -3 -4 -5]
      (vmap (fn [x] (* x x)) [1 2 3 4 5]) => [1 4 9 16 25]
      (vmap #(count %) [[1 2] ["a" "b" "c"] []]) => [2 3 0])

;; ## Question 3 : Réductions d'un vecteur


(declare vmax)
(declare vmax-nth)





(fact "`vmax` retourne bien l'élément maximal, et `vmax-nth` son indice."
      (vmax [2 3 5 1 2 0]) => 5
      (vmax-nth [2 3 5 1 2 0]) => 2
      (vmax []) => nil
      (vmax-nth []) => nil
      (vmax ["un" "deux" "trois" "quatre" "cinq" "six"]) => "un"
      (vmax-nth ["un" "deux" "trois" "quatre" "cinq" "six"]) => 0
      (vmax-nth [2 9 2 9]) => 1)

;; ## Question 4 : Construction d'une map

(declare mkmap)


(fact "`mkmap` construit la bonne map."
      (mkmap [[:un, 1] [:deux, 2] [:trois, 3]])
      => {:deux 2, :un 1, :trois 3}
      (mkmap []) => {})

;; ## Question 5 : Réduction d'une map

(declare mmaxv)


(fact "`mmaxv` retourne la bonne clé."
      (mmaxv {:a 1, :b 3, :c 2, :d -1}) => :b
      (mmaxv {}) => nil)

;; ## Question 6 : construction d'un ensemble

(declare premiers)


      

(fact "`premiers` retourne bien les `n` premiers premiers"
      (premiers 1) => #{2}
      (premiers 10) => #{2 3 5 29 7 11 13 17 19 23}
      (count (premiers 10)) => 10
      (last (premiers 100)) => 379)

;; ## Question 7 : Opérations ensemblistes.

(declare union)
(declare intersection)
(declare difference)




(facts "sur les opérations ensemblistes."
       (union #{1 2 4} #{2 3 4 5}) => #{1 2 3 4 5}
       (intersection #{1 2 4} #{2 3 4 5}) => #{2 4}
       (difference #{2 3 4 5} #{1 2 4}) => #{3 5}
       (union #{1 2 3} #{}) => #{1 2 3}
       (union #{} #{1 2 3}) => #{1 2 3}
       (intersection #{1 2 3} #{}) => #{}
       (intersection #{} #{1 2 3}) => #{}
       (difference #{1 2 3} #{}) => #{1 2 3}
       (difference #{} #{1 2 3}) => #{})




